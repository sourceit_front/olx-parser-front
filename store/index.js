export const state = () => ({
  ads: []
})

export const actions = {
  loadAds ({ commit }) {
    commit('saveLoadedAds', [
      {
        id: 1,
        title: 'Комната в гостинке',
        price: 3500,
        currency: 'UAH',
        date: '7 нояб',
        city: 'Харьков',
        district: 'Основянский',
        image: 'https://ireland.apollo.olxcdn.com/v1/files/b2xoka3ak05n2-UA/image;s=644x461'
      },
      {
        id: 2,
        title: 'Сдам свою 3к квартиру на Алексеевке. Хозяйка.',
        price: 400,
        currency: 'USD',
        date: 'Вчера 09:48',
        city: 'Харьков',
        district: 'Шевченковский',
        image: 'https://ireland.apollo.olxcdn.com/v1/files/71gsxdtdpiy23-UA/image;s=644x461'
      },
      {
        id: 3,
        title: 'Сдам 1к квартиру, Салтовское шоссе 262а',
        price: 5500,
        currency: 'UAH',
        date: 'Сегодня 11:58',
        city: 'Харьков',
        district: 'Холодногорский',
        image: 'https://ireland.apollo.olxcdn.com/v1/files/3xu24rp66iwx2-UA/image;s=644x461'
      }
    ])
  },
  loadAd ({ commit, state }, id) {
    const ad = state.ads.find(ad => ad.id === id)
    if (!ad) {
      commit('saveLoadAd', {
        id,
        title: 'Комната в гостинке',
        price: 3500,
        currency: 'UAH',
        date: '7 нояб',
        city: 'Харьков',
        district: 'Основянский',
        image: 'https://ireland.apollo.olxcdn.com/v1/files/b2xoka3ak05n2-UA/image;s=644x461'
      })
    }
  }
}

export const mutations = {
  saveLoadedAds (state, ads) {
    state.ads = ads
  },
  saveLoadAd (state, ad) {
    state.ads.push(ad)
  }
}

export const getters = {
  currentAd (state) {
    return (id) => {
      return state.ads.find(ad => ad.id === id)
    }
  }
}
