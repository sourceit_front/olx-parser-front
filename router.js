import Vue from 'vue'
import Router from 'vue-router'

import HomePage from '~/pages'
import LoginPage from '~/pages/auth/login'
import RegisterPage from '~/pages/auth/register'
import AdsPage from '~/pages/ads'
import SettingsPage from '~/pages/settings'
import AdsDetailPage from '~/pages/ads-detail'

Vue.use(Router)

export function createRouter () {
  return new Router({
    mode: 'history',
    routes: [
      {
        path: '/login',
        name: 'login',
        component: LoginPage
      },
      {
        path: '/register',
        name: 'register',
        component: RegisterPage
      },
      {
        path: '/ads',
        name: 'ads',
        component: AdsPage
      },
      {
        path: '/ads/:id',
        name: 'ads-detail',
        component: AdsDetailPage
      },
      {
        path: '/settings',
        name: 'settings',
        component: SettingsPage
      },
      {
        path: '/',
        name: 'home',
        component: HomePage
      }
    ]
  })
}
